MSBD
=============

This is a BEAST 2 package which allows for the inference of a multi-state birth-death prior on phylogenetic trees, with state-specific birth and death rates.  
The multi-states birth-death model allows the inference of the number of states, the position of states on the phylogeny, the state-specific birth and death rates and the overall rate of state change from serially-sampled sequence data. 


License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.

The following libraries are bundled with MSBD:

* Google Guava (http://code.google.com/p/guava-libraries/)
* jblas (http://mikiobraun.github.io/jblas/)

That software is distributed under the licences provided in the
LICENCE.* files included in this archive.
