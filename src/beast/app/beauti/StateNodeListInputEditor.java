package beast.app.beauti;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import javax.swing.Box;
import javax.swing.JLabel;

import beast.app.beauti.BeautiDoc;
import beast.app.draw.InputEditor;
import beast.app.draw.ListInputEditor;
import beast.app.draw.SmallLabel;
import beast.core.BEASTInterface;
import beast.core.Input;
import beast.core.StateNode;

public class StateNodeListInputEditor extends ListInputEditor {
	private static final long serialVersionUID = 1L;

	public StateNodeListInputEditor(BeautiDoc doc) {
		super(doc);
	}

	@Override
	public Class<?> type() {
		return List.class;
	}

	@Override
	public Class<?> baseType() {
		return StateNode.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int itemNr, ExpandOption bExpandOption,
			boolean bAddButtons) {
		m_buttonStatus = ButtonStatus.NONE;
		super.init(input, plugin, itemNr, bExpandOption, bAddButtons);
	}

	@Override
	protected void addSingleItem(BEASTInterface plugin) {
		Box itemBox = Box.createHorizontalBox();
		String sName = plugin.getID();
		if (sName == null || sName.length() == 0) {
			sName = plugin.getClass().getName();
			sName = sName.substring(sName.lastIndexOf('.') + 1);
		}
		JLabel label = new JLabel(sName);

		itemBox.add(Box.createRigidArea(new Dimension(5, 1)));
		itemBox.add(label);

		SmallLabel validateLabel = new SmallLabel("x", new Color(200, 0, 0));
		itemBox.add(validateLabel);
		validateLabel.setVisible(true);
		m_validateLabels.add(validateLabel);

		if (m_bExpandOption == ExpandOption.TRUE || m_bExpandOption == ExpandOption.TRUE_START_COLLAPSED
				|| (m_bExpandOption == ExpandOption.IF_ONE_ITEM && ((List<?>) m_input.get()).size() == 1)) {
			int listItemNr = ((List) m_input.get()).indexOf(plugin);
			InputEditor tmpeditor = null;
			try {
				tmpeditor = doc.getInputEditorFactory().createInputEditor(m_input, listItemNr, plugin, false,
						ExpandOption.FALSE, ButtonStatus.ALL, null, doc);
				itemBox.add(tmpeditor.getComponent());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		if (m_validateLabel == null) {
			m_listBox.add(itemBox);
		} else {
			Component c = m_listBox.getComponent(m_listBox.getComponentCount() - 1);
			m_listBox.remove(c);
			m_listBox.add(itemBox);
			m_listBox.add(c);
		}
	} // addSingleItem
}
