/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.app.multiratetree.beauti;

import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;

import beast.app.beauti.BeautiDoc;
import beast.core.BEASTInterface;
import beast.core.Input;
import beast.evolution.tree.ExpStateChangeModel;
import beast.evolution.tree.StateChangeModel;

/**
 * Shows and allows editing of an ExpStateChangeModel in Beauti.
 */
public class ExpStateChangeModelInputEditor extends StateChangeModelInputEditor {
	private static final long serialVersionUID = 565353452284692080L;

	ExpStateChangeModel expscModel;
	DefaultTableModel lambdaRatesModel, muRatesModel;
	JCheckBox lambdaDecayCheckBox, muDecayCheckBox, lambdaREstCheckBox, muREstCheckBox, lambdaRFixedCheckBox, muRFixedCheckBox;

	/**
	 * Instantiates a new exp rate shift model input editor.
	 *
	 * @param doc the beauti doc
	 */
	public ExpStateChangeModelInputEditor(BeautiDoc doc) {
		super(doc);
		expChangeInProgress = true;
	}

	@Override
	public Class<?> type() {
		return ExpStateChangeModel.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int itemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		// Set up fields
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = plugin;
		this.itemNr = itemNr;

		// Adds label to left of input editor
		addInputLabel();

		// Create component models and fill them with data from input
		expscModel = (ExpStateChangeModel) input.get();
		scModel = (StateChangeModel) input.get();
		nStatesModel = new SpinnerNumberModel(2, 1, Short.MAX_VALUE, 1);
		lambdasModel = new DefaultTableModel();
		musModel = new DefaultTableModel();

		lambdaEstCheckBox = new JCheckBox("estimate");
		muEstCheckBox = new JCheckBox("estimate");
		nStatesEstCheckBox = new JCheckBox("estimate");
		muFixedCheckBox = new JCheckBox("identical across states");
		lambdaFixedCheckBox = new JCheckBox("identical across states");

		lambdaRatesModel = new DefaultTableModel();
		muRatesModel = new DefaultTableModel();

		lambdaDecayCheckBox = new JCheckBox("Activate birth rate decay");
		muDecayCheckBox = new JCheckBox("Activate death rate decay");
		lambdaREstCheckBox = new JCheckBox("estimate");
		muREstCheckBox = new JCheckBox("estimate");
		muRFixedCheckBox = new JCheckBox("identical across states");
		lambdaRFixedCheckBox = new JCheckBox("identical across states");

		loadFromStateChangeModel();

		mainBox = Box.createVerticalBox();
		mainBox.setBorder(new EtchedBorder());

		addGammaNSTField(bExpandOption, bAddButtons);

		addLMFields();

		addEndFields(bExpandOption, bAddButtons);

		add(mainBox);

		// Spinner handler
		dimSpinner.addChangeListener((ChangeEvent e) -> {
			JSpinner spinner = (JSpinner) e.getSource();
			int newDim = (int) spinner.getValue();

			dimChangeInProgress = true;

			if (!expscModel.isLambdaFixed.get()) {
				lambdasModel.setColumnCount(newDim);
				expscModel.lambdasInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (lambdasModel.getValueAt(0, i) == null) {
						lambdasModel.setValueAt(0.0, 0, i);
					}
				}
			}

			if (!expscModel.isMuFixed.get()) {
				musModel.setColumnCount(newDim);
				expscModel.musInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (musModel.getValueAt(0, i) == null) {
						musModel.setValueAt(0.0, 0, i);
					}
				}
			}

			if (!expscModel.isLambdaRateFixed.get()) {
				lambdaRatesModel.setColumnCount(newDim);
				expscModel.lambdaRatesInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (lambdaRatesModel.getValueAt(0, i) == null) {
						lambdaRatesModel.setValueAt(0.0, 0, i);
					}
				}
			}

			if (!expscModel.isMuRateFixed.get()) {
				muRatesModel.setColumnCount(newDim);
				expscModel.muRatesInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (muRatesModel.getValueAt(0, i) == null) {
						muRatesModel.setValueAt(0.0, 0, i);
					}
				}
			}

			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		addEventListeners();
	}

	@Override
	public void loadFromStateChangeModel() {
		int n;
		try {
			n = expscModel.getNStates();
		} catch (Exception ex) { // workaround if nstates gets out of sync
			expscModel.isLambdaRateFixed.setValue(true, expscModel);
			expscModel.isMuRateFixed.setValue(true, expscModel);
			try {
				expscModel.initAndValidate();
			} catch (Exception e) {
				e.printStackTrace();
			}
			n = expscModel.getNStates();
		}

		nStatesModel.setValue(n);
		lambdasModel.setRowCount(1);
		musModel.setRowCount(1);

		if (expscModel.isLambdaFixed.get()) lambdasModel.setColumnCount(1);
		else lambdasModel.setColumnCount(n);
		if (expscModel.isMuFixed.get()) musModel.setColumnCount(1);
		else musModel.setColumnCount(n);

		for (int i = 0; i < lambdasModel.getColumnCount(); i++)
			lambdasModel.setValueAt(expscModel.getLambda(i), 0, i);
		for (int i = 0; i < musModel.getColumnCount(); i++)
			musModel.setValueAt(expscModel.getMu(i), 0, i);

		lambdaEstCheckBox.setSelected(expscModel.lambdasInput.get().isEstimatedInput.get());
		muEstCheckBox.setSelected(expscModel.musInput.get().isEstimatedInput.get());
		lambdaFixedCheckBox.setSelected(expscModel.isLambdaFixed.get());
		muFixedCheckBox.setSelected(expscModel.isMuFixed.get());

		lambdaRatesModel.setRowCount(1);
		muRatesModel.setRowCount(1);

		lambdaDecayCheckBox.setSelected(expscModel.isLambdaDecay.get());
		muDecayCheckBox.setSelected(expscModel.isMuDecay.get());

		if (lambdaDecayCheckBox.isSelected()) {
			if (expscModel.isLambdaRateFixed.get()) lambdaRatesModel.setColumnCount(1);
			else lambdaRatesModel.setColumnCount(n);
			for (int i = 0; i < lambdaRatesModel.getColumnCount(); i++)
				lambdaRatesModel.setValueAt(expscModel.getLambdaRate(i), 0, i);
			lambdaREstCheckBox.setSelected(expscModel.lambdaRatesInput.get().isEstimatedInput.get());
			lambdaRFixedCheckBox.setSelected(expscModel.isLambdaRateFixed.get());
		}

		if (muDecayCheckBox.isSelected()) {
			if (expscModel.isMuRateFixed.get()) muRatesModel.setColumnCount(1);
			else muRatesModel.setColumnCount(n);
			for (int i = 0; i < muRatesModel.getColumnCount(); i++)
				muRatesModel.setValueAt(expscModel.getMuRate(i), 0, i);
			muREstCheckBox.setSelected(expscModel.muRatesInput.get().isEstimatedInput.get());
			muRFixedCheckBox.setSelected(expscModel.isMuRateFixed.get());
		}

		nStatesEstCheckBox.setSelected(expscModel.estimateNStatesInput.get());
	}

	@Override
	public void saveToStateChangeModel() {
		expscModel.isLambdaFixed.setValue(lambdaFixedCheckBox.isSelected(), expscModel);
		expscModel.isMuFixed.setValue(muFixedCheckBox.isSelected(), expscModel);

		StringBuilder sblambdas = new StringBuilder();
		int lc = (expscModel.isLambdaFixed.get()) ? 1 : lambdasModel.getColumnCount();
		for (int i = 0; i < lc; i++) {
			if (i > 0) sblambdas.append(" ");
			if (lambdasModel.getValueAt(0, i) != null) sblambdas.append(lambdasModel.getValueAt(0, i));
			else sblambdas.append("0.0");
		}
		expscModel.lambdasInput.get().setDimension(lambdasModel.getColumnCount());
		expscModel.lambdasInput.get().valuesInput.setValue(sblambdas.toString(), expscModel.lambdasInput.get());

		int mc = (expscModel.isMuFixed.get()) ? 1 : musModel.getColumnCount();
		StringBuilder sbmus = new StringBuilder();
		for (int i = 0; i < mc; i++) {
			if (i > 0) sbmus.append(" ");
			if (musModel.getValueAt(0, i) != null) sbmus.append(musModel.getValueAt(0, i));
			else sbmus.append("0.0");
		}
		expscModel.musInput.get().setDimension(musModel.getColumnCount());
		expscModel.musInput.get().valuesInput.setValue(sbmus.toString(), expscModel.musInput.get());

		expscModel.lambdasInput.get().isEstimatedInput.setValue(lambdaEstCheckBox.isSelected(), expscModel.lambdasInput.get());
		expscModel.musInput.get().isEstimatedInput.setValue(muEstCheckBox.isSelected(), expscModel.musInput.get());

		expscModel.isLambdaDecay.setValue(lambdaDecayCheckBox.isSelected(), expscModel);
		expscModel.lambdaRatesInput.get().isEstimatedInput.setValue(lambdaREstCheckBox.isSelected(), expscModel.lambdaRatesInput.get());
		if (lambdaDecayCheckBox.isSelected()) {
			expscModel.isLambdaRateFixed.setValue(lambdaRFixedCheckBox.isSelected(), expscModel);

			StringBuilder sblambdaRates = new StringBuilder();
			int lrc = (expscModel.isLambdaRateFixed.get()) ? 1 : lambdaRatesModel.getColumnCount();
			for (int i = 0; i < lrc; i++) {
				if (i > 0) sblambdaRates.append(" ");
				if (lambdaRatesModel.getValueAt(0, i) != null) sblambdaRates.append(lambdaRatesModel.getValueAt(0, i));
				else sblambdaRates.append("0.0");
			}
			expscModel.lambdaRatesInput.get().setDimension(lambdaRatesModel.getColumnCount());
			expscModel.lambdaRatesInput.get().valuesInput.setValue(sblambdaRates.toString(), expscModel.lambdaRatesInput.get());
		}

		expscModel.isMuDecay.setValue(muDecayCheckBox.isSelected(), expscModel);
		expscModel.muRatesInput.get().isEstimatedInput.setValue(muREstCheckBox.isSelected(), expscModel.muRatesInput.get());
		if (muDecayCheckBox.isSelected()) {
			expscModel.isMuRateFixed.setValue(muRFixedCheckBox.isSelected(), expscModel);

			int mrc = (expscModel.isMuRateFixed.get()) ? 1 : muRatesModel.getColumnCount();
			StringBuilder sbmuRates = new StringBuilder();
			for (int i = 0; i < mrc; i++) {
				if (i > 0) sbmuRates.append(" ");
				if (muRatesModel.getValueAt(0, i) != null) sbmuRates.append(muRatesModel.getValueAt(0, i));
				else sbmuRates.append("0.0");
			}
			expscModel.muRatesInput.get().setDimension(muRatesModel.getColumnCount());
			expscModel.muRatesInput.get().valuesInput.setValue(sbmuRates.toString(), expscModel.muRatesInput.get());
		}

		try {
			expscModel.musInput.get().initAndValidate();
			expscModel.lambdasInput.get().initAndValidate();
			expscModel.muRatesInput.get().initAndValidate();
			expscModel.lambdaRatesInput.get().initAndValidate();
			expscModel.initAndValidate();
		} catch (Exception ex) {
			System.err.println("Error saving the exp rate shift model state: " + ex.getMessage());
		}

		refreshPanel();
	}

	@Override
	protected void addLMFields() {
		super.addLMFields();

		Box horBox = Box.createHorizontalBox();
		lambdaDecayCheckBox.setSelected(expscModel.isLambdaDecay.get());
		lambdaDecayCheckBox.setToolTipText("Use exponential decay on birth rates");
		horBox.add(lambdaDecayCheckBox);
		horBox.add(Box.createHorizontalGlue());
		horBox.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 5));
		mainBox.add(horBox);

		// lambdaRates table
		if (lambdaDecayCheckBox.isSelected()) {
			horBox = Box.createHorizontalBox();
			horBox = addInputLabel(horBox, expscModel.lambdaRatesInput);

			JTable lambdaRatesTable = new JTable(lambdaRatesModel);
			lambdaRatesTable.setCellSelectionEnabled(true);
			lambdaRatesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			for (int i = 0; i < lambdaRatesTable.getColumnCount(); i++)
				lambdaRatesTable.getColumnModel().getColumn(i).setMaxWidth(100);
			horBox.add(lambdaRatesTable);
			horBox.add(Box.createHorizontalGlue());
			lambdaREstCheckBox.setSelected(expscModel.lambdaRatesInput.get().isEstimatedInput.get());
			horBox.add(lambdaREstCheckBox);
			horBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
			mainBox.add(horBox);

			horBox = Box.createHorizontalBox();
			lambdaRFixedCheckBox.setSelected(expscModel.isLambdaRateFixed.get());
			lambdaRFixedCheckBox.setToolTipText(expscModel.isLambdaRateFixed.getHTMLTipText());
			horBox.add(lambdaRFixedCheckBox);
			horBox.add(Box.createHorizontalGlue());
			horBox.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
			mainBox.add(horBox);
		}

		horBox = Box.createHorizontalBox();
		muDecayCheckBox.setSelected(expscModel.isMuDecay.get());
		muDecayCheckBox.setToolTipText("Use exponential decay on death rates");
		horBox.add(muDecayCheckBox);
		horBox.add(Box.createHorizontalGlue());
		horBox.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 5));
		mainBox.add(horBox);

		// lambdaRates table
		if (muDecayCheckBox.isSelected()) {
			// muRates table
			horBox = Box.createHorizontalBox();
			horBox = addInputLabel(horBox, expscModel.muRatesInput);

			JTable muRatesTable = new JTable(muRatesModel);
			muRatesTable.setCellSelectionEnabled(true);
			muRatesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			for (int i = 0; i < muRatesTable.getColumnCount(); i++)
				muRatesTable.getColumnModel().getColumn(i).setMaxWidth(100);
			horBox.add(muRatesTable);
			horBox.add(Box.createHorizontalGlue());
			muREstCheckBox.setSelected(expscModel.muRatesInput.get().isEstimatedInput.get());
			horBox.add(muREstCheckBox);
			horBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
			mainBox.add(horBox);

			horBox = Box.createHorizontalBox();
			muRFixedCheckBox.setSelected(expscModel.isMuRateFixed.get());
			muRFixedCheckBox.setToolTipText(expscModel.isMuRateFixed.getHTMLTipText());
			horBox.add(muRFixedCheckBox);
			horBox.add(Box.createHorizontalGlue());
			horBox.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
			mainBox.add(horBox);
		}
	}

	@Override
	protected void addEventListeners() {
		super.addEventListeners();

		lambdaRatesModel.addTableModelListener((TableModelEvent e) -> {
			if (e.getType() != TableModelEvent.UPDATE) return;

			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		lambdaREstCheckBox.addItemListener((ItemEvent e) -> {
			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		lambdaRFixedCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			int dim = (int) nStatesModel.getNumber();

			if (e.getStateChange() == ItemEvent.DESELECTED) {
				lambdaRatesModel.setColumnCount(dim);
				expscModel.lambdaRatesInput.get().setDimension(dim);
				for (int i = 0; i < dim; i++) {
					if (lambdaRatesModel.getValueAt(0, i) == null) {
						lambdaRatesModel.setValueAt(0.0, 0, i);
					}
				}
			}
			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		muRatesModel.addTableModelListener((TableModelEvent e) -> {
			if (e.getType() != TableModelEvent.UPDATE) return;

			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		muREstCheckBox.addItemListener((ItemEvent e) -> {
			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		muRFixedCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			int dim = (int) nStatesModel.getNumber();

			if (e.getStateChange() == ItemEvent.DESELECTED) {
				muRatesModel.setColumnCount(dim);
				expscModel.muRatesInput.get().setDimension(dim);
				for (int i = 0; i < dim; i++) {
					if (muRatesModel.getValueAt(0, i) == null) {
						muRatesModel.setValueAt(0.0, 0, i);
					}
				}
			}
			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		lambdaDecayCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			if (e.getStateChange() == ItemEvent.SELECTED) {
				lambdaREstCheckBox.setSelected(true); // reactivate priors and operators
				int dim = (lambdaRFixedCheckBox.isSelected()) ? 1 : (int) nStatesModel.getNumber();
				lambdaRatesModel.setColumnCount(dim);
				for (int i = 0; i < dim; i++) {
					if (lambdaRatesModel.getValueAt(0, i) == null) {
						lambdaRatesModel.setValueAt(0.0, 0, i);
					}
				}
			} else lambdaREstCheckBox.setSelected(false); // deactivate priors and operators

			saveToStateChangeModel();
		});

		muDecayCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			if (e.getStateChange() == ItemEvent.SELECTED) {
				muREstCheckBox.setSelected(true); // reactivate priors and operators
				int dim = (muRFixedCheckBox.isSelected()) ? 1 : (int) nStatesModel.getNumber();
				muRatesModel.setColumnCount(dim);
				for (int i = 0; i < dim; i++) {
					if (muRatesModel.getValueAt(0, i) == null) {
						muRatesModel.setValueAt(0.0, 0, i);
					}
				}
			} else muREstCheckBox.setSelected(false); // deactivate priors and operators

			dimChangeInProgress = false;

			saveToStateChangeModel();
		});
	}
}
