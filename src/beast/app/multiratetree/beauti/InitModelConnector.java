/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.app.multiratetree.beauti;

import multiratetree.distributions.ExpBirthDeathDensity;
import multiratetree.distributions.MultiRateTreeDistribution;
import multiratetree.distributions.StdBirthDeathDensity;
import beast.app.beauti.BeautiDoc;
import beast.core.BEASTInterface;
import beast.evolution.likelihood.GenericTreeLikelihood;
import beast.evolution.tree.ExpStateChangeModel;
import beast.evolution.tree.MultiRateTree;
import beast.evolution.tree.MultiRateTreeFromBDModel;
import beast.evolution.tree.StateChangeModel;

/**
 * This connector avoids a problem with circular linkage scModel->initial tree->tree which led to issues with
 * storing/restoring the tree state.
 */
public class InitModelConnector {
	
	// this will be modified by the MRTDistributionInputEditor if we switch models
	public static String latest = "stdBDMRT";

	/**
	 * Custom connector.
	 *
	 * @param doc the doc
	 * @return true, if successful
	 */
	public static boolean customConnector(BeautiDoc doc) {
		for (BEASTInterface p : doc.getPartitions("Tree")) {
			GenericTreeLikelihood treeLikelihood = (GenericTreeLikelihood) p;
			MultiRateTree tree = (MultiRateTree) treeLikelihood.treeInput.get();

			if (!(tree.m_initial.get() instanceof MultiRateTreeFromBDModel)) return true;

			String pID = BeautiDoc.parsePartition(tree.getID());
			StateChangeModel initModel;
			MultiRateTreeDistribution distr = (MultiRateTreeDistribution) doc.pluginmap.get(latest + ".t:" + pID);
			if (distr instanceof StdBirthDeathDensity) initModel = ((StdBirthDeathDensity) distr).stateChangeModelInput.get();
			else initModel = ((ExpBirthDeathDensity) distr).stateChangeModelInput.get();

			StateChangeModel copyModel = ((MultiRateTreeFromBDModel) tree.m_initial.get()).stateChangeModelInput.get();

			/*
			 * if (initModel instanceof ExpStateChangeModel) { ExpStateChangeModel
			 * copyEModel, tmp = (ExpStateChangeModel) initModel; if (!(copyModel instanceof
			 * ExpStateChangeModel)) copyEModel = new ExpStateChangeModel(); else copyEModel
			 * = (ExpStateChangeModel) copyModel;
			 * 
			 * copyEModel.lambdaRatesInput.get().valuesInput.set(VectorToInputString(tmp.
			 * getLambdaRates())); copyEModel.lambdaRatesInput.get().initAndValidate();
			 * copyEModel.muRatesInput.get().valuesInput.set(VectorToInputString(tmp.
			 * getMuRates())); copyEModel.muRatesInput.get().initAndValidate();
			 * 
			 * copyEModel.setInputValue("isLambdaDecay", tmp.isLambdaDecay.get());
			 * copyEModel.setInputValue("isMuDecay", tmp.isMuDecay.get());
			 * copyEModel.setInputValue("isLambdaRateFixed", tmp.isLambdaRateFixed.get());
			 * copyEModel.setInputValue("isMuRateFixed", tmp.isMuRateFixed.get());
			 * 
			 * copyModel = copyEModel; } else { if (copyModel instanceof
			 * ExpStateChangeModel) copyModel = new StateChangeModel(); }
			 */
			
			copyModel.lambdasInput.get().valuesInput.set(VectorToInputString(initModel.getLambdas()));
			copyModel.lambdasInput.get().initAndValidate();
			copyModel.musInput.get().valuesInput.set(VectorToInputString(initModel.getMus()));
			copyModel.musInput.get().initAndValidate();
			
			copyModel.gammaInput.get().valuesInput.set(Double.toString(initModel.getGamma()));
			copyModel.nstarInput.get().valuesInput.set(Integer.toString(initModel.getNstar()));

			copyModel.setInputValue("isLambdaFixed", initModel.isLambdaFixed.get());
			copyModel.setInputValue("isMuFixed", initModel.isMuFixed.get());

			copyModel.setInputValue("rho", initModel.rhoInput.get());
			copyModel.setInputValue("sigma", initModel.sigmaInput.get());

			copyModel.setID("initModel.t:" + pID);
			copyModel.initAndValidate();

			tree.m_initial.get().setInputValue("stateChangeModel", copyModel);
		}

		return true;
	}

	private static String VectorToInputString(Double[] vector) {
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < vector.length; i++) {
			if(i > 0) result.append(" ");
			result.append(vector[i]);
		}
		return result.toString();
	}
}
