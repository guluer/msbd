/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.app.multiratetree.beauti;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import multiratetree.distributions.MultiRateTreeDistribution;
import beast.app.beauti.BeautiDoc;
import beast.app.beauti.BeautiSubTemplate;
import beast.app.beauti.PartitionContext;
import beast.app.beauti.PriorListInputEditor;
import beast.app.draw.InputEditor;
import beast.app.draw.SmallLabel;
import beast.core.BEASTInterface;
import beast.core.Input;

/**
 * Shows and allows editing of an MRT Distribution in Beauti. Allows switching between subtemplates.
 */
public class MRTDistributionInputEditor extends InputEditor.Base {

	private static final long serialVersionUID = -1334027365455261101L;

	ActionEvent m_e;

	/**
	 * Instantiates a new MRT distribution input editor.
	 *
	 * @param doc the beauti doc
	 */
	public MRTDistributionInputEditor(BeautiDoc doc) {
		super(doc);
	}

	@Override
	public Class<?> type() {
		return MultiRateTreeDistribution.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int listItemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = plugin;
		this.itemNr = listItemNr;

		Box itemBox = Box.createHorizontalBox();

		MultiRateTreeDistribution distr = (MultiRateTreeDistribution) plugin;
		String sText = distr.mrTreeInput.get().getID() + " Prior";

		JLabel label = new JLabel(sText);
		label.setMinimumSize(PriorListInputEditor.PREFERRED_SIZE);
		label.setPreferredSize(PriorListInputEditor.PREFERRED_SIZE);
		itemBox.add(label);

		List<BeautiSubTemplate> sAvailablePlugins = doc.getInputEditorFactory().getAvailableTemplates(m_input, m_beastObject, null, doc);

		JComboBox<?> comboBox = new JComboBox<>(sAvailablePlugins.toArray());
		comboBox.setName("MultiRateTreeDistribution");

		String sID = distr.getID();
		try {
			sID = sID.substring(0, sID.indexOf('.'));
		} catch (Exception e) {
			throw new RuntimeException("Improperly formatted ID: " + distr.getID());
		}
		for (BeautiSubTemplate template : sAvailablePlugins) {
			if (template.matchesName(sID)) { // getMainID().replaceAll(".\\$\\(n\\)",
				// "").equals(sID)) {
				comboBox.setSelectedItem(template);
			}
		}

		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m_e = e;
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JComboBox<?> currentComboBox = (JComboBox<?>) m_e.getSource();
						@SuppressWarnings("unchecked")
						List<BEASTInterface> list = (List<BEASTInterface>) m_input.get();
						BeautiSubTemplate template = (BeautiSubTemplate) currentComboBox.getSelectedItem();
						PartitionContext partitionContext = doc.getContextFor(list.get(itemNr));
						try {
							InitModelConnector.latest = template.getMainID().substring(0, template.getMainID().indexOf('.'));
							template.createSubNet(partitionContext, list, itemNr, true);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						sync();
						refreshPanel();
					}
				});
			}
		});
		itemBox.add(comboBox);
		itemBox.add(Box.createGlue());

		m_validateLabel = new SmallLabel("x", new Color(200, 0, 0));
		m_validateLabel.setVisible(false);
		validateInput();
		itemBox.add(m_validateLabel);
		add(itemBox);
	}

	@Override
	public void validateInput() {
		super.validateInput();
	}

}
