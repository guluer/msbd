/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.app.multiratetree.beauti;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

import beast.app.beauti.BeautiDoc;
import beast.app.beauti.BeautiSubTemplate;
import beast.app.beauti.PartitionContext;
import beast.app.beauti.TipDatesInputEditor;
import beast.app.draw.InputEditor;
import beast.app.draw.SmallLabel;
import beast.core.BEASTInterface;
import beast.core.Input;
import beast.evolution.tree.MultiRateTree;

/**
 * Shows and allows editing of a MultiRateTree in Beauti. Allows switching starting tree templates.
 */
public class MRTreeInputEditor extends InputEditor.Base {
	private static final long serialVersionUID = -1334027365455261101L;

	ActionEvent m_e;
	BeautiSubTemplate current;

	/**
	 * Instantiates a new MultiRateTree input editor.
	 *
	 * @param doc the beauti doc
	 */
	public MRTreeInputEditor(BeautiDoc doc) {
		super(doc);
	}

	@Override
	public Class<?> type() {
		return MultiRateTree.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int listItemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = plugin;
		this.itemNr = listItemNr;
		Box mbox = Box.createVerticalBox();

		Box itemBox = Box.createHorizontalBox();
		List<BeautiSubTemplate> sAvailablePlugins = doc.getInputEditorFactory().getAvailableTemplates(m_input, m_beastObject, null, doc);

		MultiRateTree tree;
		if (itemNr < 0) {
			tree = (MultiRateTree) m_input.get();
		} else {
			tree = (MultiRateTree) ((List) m_input.get()).get(itemNr);
		}
		JComboBox<?> comboBox = new JComboBox<>(sAvailablePlugins.toArray());
		comboBox.setName("MultiRateTree");

		String sID = tree.m_initial.get().getID();
		try {
			sID = sID.substring(0, sID.indexOf('.'));
		} catch (Exception e) {
			throw new RuntimeException("Improperly formatted ID: " + sID);
		}
		for (BeautiSubTemplate template : sAvailablePlugins) {
			if (template.matchesName(sID)) {
				comboBox.setSelectedItem(template);
				current = template;
			}
		}
		itemBox.add(comboBox);

		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m_e = e;
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JComboBox<?> currentComboBox = (JComboBox<?>) m_e.getSource();
						BeautiSubTemplate template = (BeautiSubTemplate) currentComboBox.getSelectedItem();
						PartitionContext partitionContext;
						List<BEASTInterface> list = null;
						if (itemNr > -1) {
							list = (List<BEASTInterface>) m_input.get();
							partitionContext = doc.getContextFor(list.get(itemNr));
						} else partitionContext = doc.getContextFor((BEASTInterface) m_input.get());
						try {
							template.removeSubNet(current, partitionContext);
							MultiRateTree tmp = (MultiRateTree) template.createSubNet(partitionContext, true);
							if (itemNr > -1) list.get(listItemNr).setInputValue("initial", tmp);
							else ((BEASTInterface) m_input.get()).setInputValue("initial", tmp);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						sync();
						refreshPanel();
					}
				});
			}
		});
		// }

		itemBox.add(Box.createHorizontalGlue());

		m_validateLabel = new SmallLabel("x", new Color(200, 0, 0));
		m_validateLabel.setVisible(false);
		validateInput();
		itemBox.add(m_validateLabel);

		mbox.add(itemBox);

		Box inputbox = Box.createVerticalBox();
		doc.getInputEditorFactory().addInputs(inputbox, tree, this, this, doc);
		doc.getInputEditorFactory().addInputs(inputbox, tree.m_initial.get(), null, this, doc);
		mbox.add(inputbox);

		TipDatesInputEditor editor = new TipDatesInputEditor(doc);
		editor.init(m_input, tree, listItemNr, bExpandOption, bAddButtons);
		mbox.add(editor.getComponent());
		mbox.add(Box.createVerticalStrut(15));
		mbox.add(Box.createVerticalGlue());
		add(mbox);
	}

	@Override
	public void validateInput() {
		super.validateInput();

		MultiRateTree tree;
		if (itemNr < 0) {
			tree = (MultiRateTree) m_input.get();
		} else {
			tree = (MultiRateTree) ((List) m_input.get()).get(itemNr);
		}

		tree.m_initial.get().initAndValidate();
		tree.initAndValidate();

		sync();
		refreshPanel();
	}
}
