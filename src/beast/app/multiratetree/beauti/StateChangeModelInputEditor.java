/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.app.multiratetree.beauti;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;

import beast.app.beauti.BeautiDoc;
import beast.app.draw.DoubleInputEditor;
import beast.app.draw.InputEditor;
import beast.app.draw.ParameterInputEditor;
import beast.core.BEASTInterface;
import beast.core.Input;
import beast.core.util.Log;
import beast.evolution.tree.StateChangeModel;

/**
 * Shows and allows editing of a StateChangeModel in Beauti.
 */
public class StateChangeModelInputEditor extends InputEditor.Base {

	private static final long serialVersionUID = -1971090631460150468L;

	DefaultTableModel lambdasModel, musModel;
	SpinnerNumberModel nStatesModel;
	StateChangeModel scModel;

	JCheckBox lambdaEstCheckBox, muEstCheckBox, nStatesEstCheckBox, lambdaFixedCheckBox, muFixedCheckBox;
	InputEditor.Base gammaInputEditor, rhoInputEditor, sigmaInputEditor, nstarInputEditor;

	// for extension to exp models
	Box mainBox;
	JSpinner dimSpinner;

	boolean dimChangeInProgress = false;
	boolean expChangeInProgress = false; // if this editor is part of an exp input editor, some operations are left to
	// the inherited class

	/**
	 * Instantiates a new rate shift model input editor.
	 *
	 * @param doc the beauti doc
	 */
	public StateChangeModelInputEditor(BeautiDoc doc) {
		super(doc);
		gammaInputEditor = new ParameterInputEditor(doc);
		rhoInputEditor = new DoubleInputEditor(doc);
		sigmaInputEditor = new DoubleInputEditor(doc);
		nstarInputEditor = new ParameterInputEditor(doc);
	}

	@Override
	public Class<?> type() {
		return StateChangeModel.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int itemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		// Set up fields
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = plugin;
		this.itemNr = itemNr;

		// Adds label to left of input editor
		addInputLabel();

		// Create component models and fill them with data from input
		scModel = (StateChangeModel) input.get();
		nStatesModel = new SpinnerNumberModel(2, 1, Short.MAX_VALUE, 1);
		lambdasModel = new DefaultTableModel();
		musModel = new DefaultTableModel();

		lambdaEstCheckBox = new JCheckBox("estimate");
		muEstCheckBox = new JCheckBox("estimate");
		nStatesEstCheckBox = new JCheckBox("estimate");
		muFixedCheckBox = new JCheckBox("identical across states");
		lambdaFixedCheckBox = new JCheckBox("identical across states");

		loadFromStateChangeModel();

		mainBox = Box.createVerticalBox();
		mainBox.setBorder(new EtchedBorder());

		addGammaNSTField(bExpandOption, bAddButtons);

		addLMFields();

		addEndFields(bExpandOption, bAddButtons);

		add(mainBox);

		// Event handlers
		dimSpinner.addChangeListener((ChangeEvent e) -> {
			JSpinner spinner = (JSpinner) e.getSource();
			int newDim = (int) spinner.getValue();

			dimChangeInProgress = true;

			if (!scModel.isLambdaFixed.get()) {
				lambdasModel.setColumnCount(newDim);
				scModel.lambdasInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (lambdasModel.getValueAt(0, i) == null) {
						lambdasModel.setValueAt(0.0, 0, i);
					}
				}
			}

			if (!scModel.isMuFixed.get()) {
				musModel.setColumnCount(newDim);
				scModel.musInput.get().setDimension(newDim);
				for (int i = 0; i < newDim; i++) {
					if (musModel.getValueAt(0, i) == null) {
						musModel.setValueAt(0.0, 0, i);
					}
				}
			}

			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		addEventListeners();
		addValidationLabel();
	}

	/**
	 * Load data from the rate shift model.
	 */
	public void loadFromStateChangeModel() {
		int n = scModel.getNStates();
		nStatesModel.setValue(n);
		lambdasModel.setRowCount(1);
		musModel.setRowCount(1);

		if (scModel.isLambdaFixed.get()) lambdasModel.setColumnCount(1);
		else lambdasModel.setColumnCount(n);
		if (scModel.isMuFixed.get()) musModel.setColumnCount(1);
		else musModel.setColumnCount(n);

		for (int i = 0; i < lambdasModel.getColumnCount(); i++)
			lambdasModel.setValueAt(scModel.getLambda(i), 0, i);
		for (int i = 0; i < musModel.getColumnCount(); i++)
			musModel.setValueAt(scModel.getMu(i), 0, i);

		lambdaEstCheckBox.setSelected(scModel.lambdasInput.get().isEstimatedInput.get());
		muEstCheckBox.setSelected(scModel.musInput.get().isEstimatedInput.get());
		lambdaFixedCheckBox.setSelected(scModel.isLambdaFixed.get());
		muFixedCheckBox.setSelected(scModel.isMuFixed.get());

		nStatesEstCheckBox.setSelected(scModel.estimateNStatesInput.get());
	}

	/**
	 * Save data to the rate shift model.
	 */
	public void saveToStateChangeModel() {
		scModel.isLambdaFixed.setValue(lambdaFixedCheckBox.isSelected(), scModel);
		scModel.isMuFixed.setValue(muFixedCheckBox.isSelected(), scModel);

		StringBuilder sblambdas = new StringBuilder();
		int lc = (scModel.isLambdaFixed.get()) ? 1 : lambdasModel.getColumnCount();
		for (int i = 0; i < lc; i++) {
			if (i > 0) sblambdas.append(" ");
			if (lambdasModel.getValueAt(0, i) != null) sblambdas.append(lambdasModel.getValueAt(0, i));
			else sblambdas.append("0.0");
		}
		scModel.lambdasInput.get().setDimension(lambdasModel.getColumnCount());
		scModel.lambdasInput.get().valuesInput.setValue(sblambdas.toString(), scModel.lambdasInput.get());

		int mc = (scModel.isMuFixed.get()) ? 1 : musModel.getColumnCount();
		StringBuilder sbmus = new StringBuilder();
		for (int i = 0; i < mc; i++) {
			if (i > 0) sbmus.append(" ");
			if (musModel.getValueAt(0, i) != null) sbmus.append(musModel.getValueAt(0, i));
			else sbmus.append("0.0");
		}
		scModel.musInput.get().setDimension(musModel.getColumnCount());
		scModel.musInput.get().valuesInput.setValue(sbmus.toString(), scModel.musInput.get());

		scModel.lambdasInput.get().isEstimatedInput.setValue(lambdaEstCheckBox.isSelected(), scModel.lambdasInput.get());
		scModel.musInput.get().isEstimatedInput.setValue(muEstCheckBox.isSelected(), scModel.musInput.get());

		validateInput();
		refreshPanel();
	}

	/**
	 * Adds the input label.
	 *
	 * @param horBox the box where to add
	 * @param input the input
	 * @return the updated box
	 */
	protected Box addInputLabel(Box horBox, Input<?> input) {
		if (m_bAddButtons) {
			String sName = formatName(input.getName());
			String sTipText = input.getHTMLTipText();
			JLabel inputLabel = new JLabel(sName);
			inputLabel.setToolTipText(sTipText);
			inputLabel.setHorizontalTextPosition(JLabel.RIGHT);
			Dimension size = new Dimension(200, 20);
			inputLabel.setMaximumSize(size);
			inputLabel.setMinimumSize(size);
			inputLabel.setPreferredSize(size);
			inputLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
			horBox.add(inputLabel);
		}
		return horBox;
	}

	/**
	 * Adds the gamma and nstates fields.
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	protected void addGammaNSTField(ExpandOption bExpandOption, boolean bAddButtons) {
		gammaInputEditor.init(scModel.gammaInput, scModel, itemNr, bExpandOption, bAddButtons);
		mainBox.add(gammaInputEditor);
		gammaInputEditor.addValidationListener(this);

		// nstates
		Box horBox = Box.createHorizontalBox();

		JLabel tmp = new JLabel("Number of states");
		tmp.setHorizontalTextPosition(JLabel.RIGHT);
		Dimension size = new Dimension(200, 20);
		tmp.setMaximumSize(size);
		tmp.setMinimumSize(size);
		tmp.setPreferredSize(size);
		tmp.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		horBox.add(tmp);

		dimSpinner = new JSpinner(nStatesModel);
		dimSpinner.setMaximumSize(new Dimension(100, Short.MAX_VALUE));
		horBox.add(dimSpinner);
		horBox.add(Box.createHorizontalGlue());
		nStatesEstCheckBox.setSelected(scModel.estimateNStatesInput.get());
		horBox.add(nStatesEstCheckBox);
		horBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
		mainBox.add(horBox);
	}

	/**
	 * Adds the end fields (rho, sigma, nstar).
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the b add buttons option
	 */
	protected void addEndFields(ExpandOption bExpandOption, boolean bAddButtons) {
		rhoInputEditor.init(scModel.rhoInput, scModel, itemNr, bExpandOption, bAddButtons);
		rhoInputEditor.addValidationListener(this);
		mainBox.add(rhoInputEditor);
		sigmaInputEditor.init(scModel.sigmaInput, scModel, itemNr, bExpandOption, bAddButtons);
		sigmaInputEditor.addValidationListener(this);
		mainBox.add(sigmaInputEditor);
		nstarInputEditor.init(scModel.nstarInput, scModel, itemNr, bExpandOption, bAddButtons);
		mainBox.add(nstarInputEditor);
		nstarInputEditor.addValidationListener(this);
	}

	/**
	 * Adds the lambda and mu fields.
	 */
	protected void addLMFields() {
		// Lambdas table
		Box horBox = Box.createHorizontalBox();
		horBox = addInputLabel(horBox, scModel.lambdasInput);

		JTable lambdasTable = new JTable(lambdasModel);
		lambdasTable.setCellSelectionEnabled(true);
		lambdasTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		for (int i = 0; i < lambdasTable.getColumnCount(); i++)
			lambdasTable.getColumnModel().getColumn(i).setMaxWidth(100);
		horBox.add(lambdasTable);
		horBox.add(Box.createHorizontalGlue());
		lambdaEstCheckBox.setSelected(scModel.lambdasInput.get().isEstimatedInput.get());
		horBox.add(lambdaEstCheckBox);
		horBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
		mainBox.add(horBox);

		horBox = Box.createHorizontalBox();
		lambdaFixedCheckBox.setSelected(scModel.isLambdaFixed.get());
		lambdaFixedCheckBox.setToolTipText(scModel.isLambdaFixed.getHTMLTipText());
		horBox.add(lambdaFixedCheckBox);
		horBox.add(Box.createHorizontalGlue());
		horBox.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		mainBox.add(horBox);

		// Mus table
		horBox = Box.createHorizontalBox();
		horBox = addInputLabel(horBox, scModel.musInput);

		JTable musTable = new JTable(musModel);
		musTable.setCellSelectionEnabled(true);
		musTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		for (int i = 0; i < musTable.getColumnCount(); i++)
			musTable.getColumnModel().getColumn(i).setMaxWidth(100);
		horBox.add(musTable);
		horBox.add(Box.createHorizontalGlue());
		muEstCheckBox.setSelected(scModel.musInput.get().isEstimatedInput.get());
		horBox.add(muEstCheckBox);
		horBox.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 5));
		mainBox.add(horBox);

		horBox = Box.createHorizontalBox();
		muFixedCheckBox.setSelected(scModel.isMuFixed.get());
		muFixedCheckBox.setToolTipText(scModel.isMuFixed.getHTMLTipText());
		horBox.add(muFixedCheckBox);
		horBox.add(Box.createHorizontalGlue());
		horBox.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		mainBox.add(horBox);
	}

	/**
	 * Adds the event listeners.
	 */
	protected void addEventListeners() {
		lambdasModel.addTableModelListener((TableModelEvent e) -> {
			if (e.getType() != TableModelEvent.UPDATE) return;

			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		lambdaEstCheckBox.addItemListener((ItemEvent e) -> {
			saveToStateChangeModel();
		});

		lambdaFixedCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			int dim = (int) nStatesModel.getNumber();

			if (e.getStateChange() == ItemEvent.DESELECTED) {
				lambdasModel.setColumnCount(dim);
				scModel.lambdasInput.get().setDimension(dim);
				for (int i = 0; i < dim; i++) {
					if (lambdasModel.getValueAt(0, i) == null) {
						lambdasModel.setValueAt(0.0, 0, i);
					}
				}
			}
			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		musModel.addTableModelListener((TableModelEvent e) -> {
			if (e.getType() != TableModelEvent.UPDATE) return;

			if (!dimChangeInProgress) saveToStateChangeModel();
		});

		muEstCheckBox.addItemListener((ItemEvent e) -> {
			saveToStateChangeModel();
		});

		muFixedCheckBox.addItemListener((ItemEvent e) -> {

			dimChangeInProgress = true;
			int dim = (int) nStatesModel.getNumber();

			if (e.getStateChange() == ItemEvent.DESELECTED) {
				musModel.setColumnCount(dim);
				scModel.musInput.get().setDimension(dim);
				for (int i = 0; i < dim; i++) {
					if (musModel.getValueAt(0, i) == null) {
						musModel.setValueAt(0.0, 0, i);
					}
				}
			}
			dimChangeInProgress = false;

			saveToStateChangeModel();
		});

		nStatesEstCheckBox.addItemListener((ItemEvent e) -> {
			scModel.estimateNStatesInput.setValue(nStatesEstCheckBox.isSelected(), scModel);
			refreshPanel();
		});
	}

	@Override
	public void validateInput() {
		try {
			scModel.musInput.get().initAndValidate();
			scModel.lambdasInput.get().initAndValidate();
			scModel.initAndValidate();
			if (m_validateLabel != null) {
                m_validateLabel.setVisible(false);
            }
		} catch (Exception e) {
			Log.err.println("Validation message: " + e.getMessage());
			if (m_validateLabel != null) {
				m_validateLabel.setToolTipText(e.getMessage());
				m_validateLabel.m_circleColor = Color.red;
				m_validateLabel.setVisible(true);
			}
			notifyValidationListeners(ValidationStatus.IS_INVALID);
		}
		repaint();
	}

}
