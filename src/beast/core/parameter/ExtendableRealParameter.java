/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.core.parameter;

import java.util.Arrays;

import beast.core.StateNode;

/**
 * This class allows to restore the state from file when the parameter dimension is variable.
 */
public class ExtendableRealParameter extends RealParameter {

	/**
	 * Instantiates a new extendable real parameter.
	 */
	public ExtendableRealParameter() {
		super();
	}

	/**
	 * Instantiates a new extendable real parameter.
	 *
	 * @param fValues the values to initialize with
	 */
	public ExtendableRealParameter(final Double[] fValues) {
		super(fValues);
	}

	@Override
	public void assignFrom(final StateNode other) {
		@SuppressWarnings("unchecked")
		final Parameter.Base<Double> source = (Parameter.Base<Double>) other;
		setID(source.getID());
		values = source.values.clone();
		storedValues = source.storedValues.clone();
		this.setDimension(source.values.length);
		System.arraycopy(source.values, 0, values, 0, source.values.length);
		m_fLower = source.m_fLower;
		m_fUpper = source.m_fUpper;
		m_bIsDirty = new boolean[source.values.length];
	}

	@Override
	public void assignFromFragile(final StateNode other) {
		@SuppressWarnings("unchecked")
		final Parameter.Base<Double> source = (Parameter.Base<Double>) other;
		this.setDimension(source.values.length);
		System.arraycopy(source.values, 0, values, 0, source.values.length);
		Arrays.fill(m_bIsDirty, false);
	}
}
