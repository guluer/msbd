/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;

import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.StateNode;
import beast.core.StateNodeInitialiser;
import beast.core.parameter.IntegerParameter;
import beast.util.Randomizer;

import com.google.common.collect.Lists;

/**
 * Initializes a multi-rate tree from a coalescent model using the parameters from the given birth-death model.
 */
public class MultiRateTreeFromBDModel extends MultiRateTree implements StateNodeInitialiser {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Rate shift model to use in simulator.", Validate.REQUIRED);

	public Input<IntegerParameter> nLeavesInput = new Input<>("nLeaves", "Number of leaf nodes.");

	public Input<String> outputFileNameInput = new Input<>("outputFileName", "Optional name of file to write simulated tree to.");

	public Input<Double> mrcaAgeInput = new Input<>("mrcaAge",
			"Scale resulting tree so that the MRCA has the chosen age.");

	/*
	 * Non-input fields:
	 */
	protected StateChangeModel stateChangeModel;
	protected int nLeaves;

	private List<String> leafNames;
	private List<Double> leafTimes;

	/**
	 * Instantiates a new multi-rate tree from birth-death model.
	 */
	public MultiRateTreeFromBDModel() {}

	@SuppressWarnings("deprecation")
	@Override
	public void initAndValidate() {

		super.initAndValidate();

		// Obtain required parameters from inputs:
		stateChangeModel = stateChangeModelInput.get();

		// Obtain leaf names from explicit input or alignment:
		leafNames = Lists.newArrayList();
		if (this.m_taxonset.get() != null) {
			nLeaves = this.m_taxonset.get().getTaxonCount();
			leafNames = this.m_taxonset.get().asStringList();
		} else {
			nLeaves = nLeavesInput.get().getValue();
			for (int i = 0; i < nLeaves; i++)
				leafNames.add(String.valueOf(i));
		}

		// Set leaf times if specified:
		leafTimes = Lists.newArrayList();
		if (timeTraitSet == null) {
			for (int i = 0; i < nLeaves; i++)
				leafTimes.add(0.0);
		} else {
			if (timeTraitSet.taxaInput.get().asStringList().size() != nLeaves)
				throw new IllegalArgumentException("Number of time traits " + "doesn't match number of leaves supplied.");

			for (int i = 0; i < nLeaves; i++)
				leafTimes.add(timeTraitSet.getValue(i));
		}

		// Construct tree
		this.root = simulateTree();
		this.root.parent = null;
		this.nodeCount = this.root.getNodeCount();
		this.internalNodeCount = this.root.getInternalNodeCount();
		this.leafNodeCount = this.root.getLeafNodeCount();
		initArrays();

		// Write tree to disk if requested
		if (outputFileNameInput.get() != null) {
			try (PrintStream pstream = new PrintStream(outputFileNameInput.get())) {
				pstream.println("#nexus\nbegin trees;");
				pstream.println("tree TREE_1 = " + toString() + ";");
				pstream.println("end;");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Simulates the tree.
	 *
	 * @return the root node
	 */
	private Node simulateTree() {
		// Initialise node creation counter:
		int nextNodeNr = 0;

		// Initialise node lists:
		List<MultiRateNode> activeNodes = Lists.newArrayList();
		List<MultiRateNode> inactiveNodes = Lists.newArrayList();

		// Add nodes to inactive nodes list:
		for (int l = 0; l < nLeaves; l++) {
			MultiRateNode node = new MultiRateNode();
			node.setNr(nextNodeNr);
			node.setID(leafNames.get(l));
			node.setHeight(leafTimes.get(l));
			if (node.height <= 1e-5) activeNodes.add(node);
			else inactiveNodes.add(node);
			node.setNodeState(0);

			nextNodeNr++;
		}

		// Sort nodes in inactive nodes lists in order of increasing age:
		Collections.sort(inactiveNodes, (MultiRateNode node1, MultiRateNode node2) -> {
			double dt = node1.getHeight() - node2.getHeight();
			if (dt < 0) return -1;
			if (dt > 0) return 1;

			return 0;
		});

		double specProp;
		double t = 0.0;

		while (activeNodes.size() > 1 || inactiveNodes.size() > 0) {

			specProp = activeNodes.size() * stateChangeModel.getLambda(0);

			// Step 3: Handle activation of nodes:
			MultiRateNode nextNode = null;
			double nextTime;
			if (specProp > 0) nextTime = t + Randomizer.nextExponential(specProp);
			else nextTime = t * 1.1;

			for (int i = 0; i < inactiveNodes.size(); i++) {
				if (inactiveNodes.get(i).getHeight() < nextTime) {
					nextNode = inactiveNodes.get(i);
					activeNodes.add(nextNode);
					inactiveNodes.remove(i);
				}
			}

			if (activeNodes.size() > 1) {
				// Step 4: Place event on tree (if more than 2 lineages).
				nextNodeNr = updateTree(activeNodes, nextTime, nextNodeNr);
			}

			// Step 5: Keep track of time increment.
			t = nextTime;
		}

		Node rootNode = activeNodes.get(0);

		if (mrcaAgeInput.get() != null) {
			double scaleFactor = mrcaAgeInput.get()/rootNode.getHeight();
			activeNodes.get(0).scale(scaleFactor);
		}

		// Return sole remaining active node as root:
		return rootNode;
	}

	/**
	 * Updates the tree with the result of the latest event.
	 *
	 * @param activeNodes the active nodes
	 * @param time the time of the event
	 * @param nextNodeNr the integer identifier of the last node added to the tree.
	 * @return the updated nextNodeNr.
	 */
	private static int updateTree(List<MultiRateNode> activeNodes, double time, int nextNodeNr) {

		// Randomly select node pair with chosen colour:
		int n = activeNodes.size();
		int n1 = Randomizer.nextInt(n);
		int n2 = Randomizer.nextInt(n);
		while (n1 == n2)
			n2 = Randomizer.nextInt(n);

		MultiRateNode daughter = activeNodes.get(n1);
		MultiRateNode son = activeNodes.get(n2);

		// Create new parent node with appropriate ID and time:
		MultiRateNode parent = new MultiRateNode();
		parent.setNr(nextNodeNr);
		parent.setID(String.valueOf(nextNodeNr));
		parent.setHeight(time);
		nextNodeNr++;

		// Connect new parent to children:
		parent.addChild(daughter);
		parent.addChild(son);
		son.setParent(parent);
		daughter.setParent(parent);

		// Ensure new parent is set to correct colour:
		parent.setNodeState(0);

		// Update activeNodes:
		activeNodes.set(n1, parent);
		activeNodes.remove(son);

		return nextNodeNr;
	}

	@Override
	public void initStateNodes() {}

	@Override
	public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
		stateNodeList.add(this);
	}

}
