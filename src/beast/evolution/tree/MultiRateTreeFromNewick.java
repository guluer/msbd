/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package beast.evolution.tree;

import java.util.List;

import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.StateNode;
import beast.core.StateNodeInitialiser;
import beast.util.TreeParser;

/**
 * This is a helper class for parsing a multi-rate tree from a newick string Colour metadata will be used if available,
 * otherwise the whole tree is set to state 0.
 */
public class MultiRateTreeFromNewick extends MultiRateTree implements StateNodeInitialiser {

	public Input<String> newickStringInput = new Input<>("newick", "Tree in Newick format.", Validate.REQUIRED);

	public Input<Boolean> adjustTipHeightsInput = new Input<>("adjustTipHeights", "Adjust tip heights in tree? Default true.", true);

	public final Input<Boolean> isLabelledNewickInput = new Input<>("isLabelledNewick", "Is the newick tree labelled (alternatively contains node numbers)? Default=false.", false);

	@Override
	public void initAndValidate() {

		super.initAndValidate();

		TreeParser parser = new TreeParser();
		parser.initByName("taxonset", m_taxonset.get(), "IsLabelledNewick", isLabelledNewickInput.get(), "adjustTipHeights", adjustTipHeightsInput.get(), "singlechild", true,
				"newick", newickStringInput.get());
		Tree flatTree = parser;

		initFromFlatTree(flatTree, isLabelledNewickInput.get());
		if (!this.isValid()) throw new IllegalStateException("Can't initialize tree with invalid colouring");
	}

	@Override
	public void initStateNodes() {}

	@Override
	public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
		stateNodeList.add(this);
	}
}
