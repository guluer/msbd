/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multiratetree.distributions;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.math.special.Gamma;

import beast.core.Input;
import beast.core.Input.Validate;
import beast.evolution.tree.ExpStateChangeModel;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.Node;

/**
 * Tree likelihood for an ExpStateChangeModel. Parameter decay is approximated by dividing edges in steps (of length
 * stepsize) and taking the average of the parameter over that step.
 */
public class ExpBirthDeathDensity extends MultiRateTreeDistribution {

	public Input<ExpStateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Model of transition between states.",
			Validate.REQUIRED);

	public Input<Double> stepsizeInput = new Input<>("stepsize", "Size of step used to approximate the exponential decay",
			Validate.REQUIRED);

	protected ExpStateChangeModel stateChangeModel;
	protected double stepsize;

	/**
	 * Instantiates a new exp birth death density.
	 */
	public ExpBirthDeathDensity() {}

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		stateChangeModel = stateChangeModelInput.get();
		stepsize = stepsizeInput.get();
		rho = stateChangeModel.getRho();
		sigma = stateChangeModel.getSigma();
	}

	@Override
	public double calculateLogP() {
		if (checkValidity && !mrTree.isValid()) return Double.NEGATIVE_INFINITY;
		if (stateChangeModel.getNstar() < stateChangeModel.getNStates()) return Double.NEGATIVE_INFINITY;

		Node rootNode = mrTree.getRoot();
		logP = 0;

		double t0 = rootNode.getHeight();
		if (!mrca) {
			if (origin.getValue() < t0) return Double.NEGATIVE_INFINITY;
			t0 = origin.getValue();
		}
		precalcPvalues(t0);

		if (!mrca) {
			double pval = ((MultiRateNode) rootNode).getPval(0);
			int rootstate = ((MultiRateNode) rootNode).getNodeState();
			double rooth = rootNode.getHeight();
			logP += this.step_q(t0, rooth, pval, rootstate, t0, stateChangeModel.getGamma());
			logP += Math.log(averagePar(stateChangeModel.getLambda(rootstate), stateChangeModel.getLambdaRate(rootstate), rooth, rooth, t0));
		}

		for (Node child : rootNode.getChildren()) {
			logP += logPNode(child, t0);
		}

		// correction for unseen states : each unseen state m+1 added adds a (nstar - m) factor
		// so full correction is (nstar - 1)! / (nstar - n)!
		// NB: Gamma(x) = (x - 1)!
		logP += Gamma.logGamma(stateChangeModel.getNstar() - 1 + 1)
				- Gamma.logGamma(stateChangeModel.getNstar() - stateChangeModel.getNStates() + 1);

		return logP;
	}

	/**
	 * Calculate logP of the subtree defined by node n.
	 *
	 * @param n the node
	 * @param tx the time when the current state started
	 * @return the logP of the subtree
	 */
	double logPNode(Node n, double tx) {
		MultiRateNode node = (MultiRateNode) n;
		double edge_ti, edge_te;
		int dwnState, state;
		double gamma = stateChangeModel.getGamma();
		double logPNode = 0;

		for (int i = node.getChangeCount() - 1; i > -2; i--) {
			if (i >= 0) edge_te = node.getChangeTime(i);
			else edge_te = node.getHeight();

			if (i >= 0) state = node.getChangeState(i);
			else state = node.getNodeState();

			if (i > 0) dwnState = node.getChangeState(i - 1);
			else dwnState = node.getNodeState();

			if (node.getChangeCount() > i + 1) edge_ti = node.getChangeTime(i + 1);
			else edge_ti = node.getParent().getHeight();

			double pval = node.getPval(i + 1);
			logPNode += this.step_q(edge_ti, edge_te, pval, state, tx, gamma);

			if (i >= 0) {
				logPNode += Math.log(stateChangeModel.getRate(dwnState, state));
				tx = edge_te;
			} else {
				if (node.isLeaf()) {
					if (edge_te > 1e-4 || !toThePresent) {
						// Extinct sample
						double m_av = averagePar(stateChangeModel.getMu(state), stateChangeModel.getMuRate(state), edge_te, edge_te, tx);
						logPNode += Math.log(m_av * sigma);
					} else {
						// Extant sample
						logPNode += Math.log(rho);
					}
				} else {
					// Next event is a speciation
					double l_av = averagePar(stateChangeModel.getLambda(state), stateChangeModel.getLambdaRate(state), edge_te, edge_te, tx);
					logPNode += Math.log(l_av);
				}
			}
		}

		for (Node child : node.getChildren()) {
			logPNode += logPNode(child, tx);
		}
		return (logPNode);
	}

	/**
	 * Average parameter over a time interval, given the parameter follows an exponential decay.
	 *
	 * @param par0 the initial parameter value
	 * @param rate the decay rate
	 * @param ti the start of the interval
	 * @param te the end of the interval (te < ti)
	 * @param t_initdecay the initial time for decay, par(t_initdecay)=par0
	 * @return the average parameter
	 */
	private static double averagePar(double par0, double rate, double ti, double te, double t_initdecay) {
		if (rate == 0) return par0;
		if (rate * (ti - te) < 1e-10) return (par0 * Math.exp(rate * (ti - t_initdecay)));
		double av = (par0 / (rate * (ti - te))) * (Math.exp(rate * (ti - t_initdecay)) - Math.exp(rate * (te - t_initdecay)));
		return av;
	}

	/**
	 * Precalculate all needed p-values for all states.
	 *
	 * @param t0 the starting time of the root state
	 */
	private void precalcPvalues(double t0) {
		for (Node n : mrTree.getNodesAsArray()) {
			MultiRateNode node = (MultiRateNode) n;

			// special case: root state
			if (node.isRoot()) {
				int state = node.getNodeState();
				ArrayList<Triple> times = new ArrayList<>();
				times = getDescendingTimes(node, 0, times);
				Collections.sort(times);
				double pval = 0, ptime = 0;
				for (Triple x : times) {
					pval = step_p0(x.e1, ptime, pval, state, t0);
					ptime = x.e1;
					((MultiRateNode) mrTree.getNode(x.e2)).setPval(x.e3, pval);
				}
			}

			if (node.getChangeCount() == 0) continue;
			for (int i = 0; i < node.getChangeCount(); i++) {
				double tx = node.getChangeTime(i);
				int state;
				if (i > 0) state = node.getChangeState(i - 1);
				else state = node.getNodeState();

				ArrayList<Triple> times = new ArrayList<>();
				times = getDescendingTimes(node, i, times);
				Collections.sort(times);

				double pval = 0, ptime = 0;
				for (Triple x : times) {
					pval = step_p0(x.e1, ptime, pval, state, tx);
					ptime = x.e1;
					((MultiRateNode) mrTree.getNode(x.e2)).setPval(x.e3, pval);
				}
			}
		}
	}

	/**
	 * Calculates the p value at time t in state, given that state started at time tx and p(ptime)=pval
	 *
	 * @param t the time
	 * @param ptime the time of the initial condition
	 * @param pval the value of the initial condition
	 * @param state the current state
	 * @param tx the starting time of state
	 * @return the new p-value
	 */
	private double step_p0(Double t, double ptime, double pval, int state, double tx) {
		int nsteps = (int) Math.ceil((t - ptime) / stepsize);
		if (nsteps == 0) return pval;

		double lambda = stateChangeModel.getLambda(state);
		double lr = stateChangeModel.getLambdaRate(state);
		double mu = stateChangeModel.getMu(state);
		double mr = stateChangeModel.getMuRate(state);

		double step = (t - ptime) / nsteps;
		double l_av, m_av, ti, te;
		for (int s = 1; s <= nsteps; s++) {
			ti = ptime + s * step;
			te = ptime + (s - 1) * step;
			l_av = averagePar(lambda, lr, ti, te, tx);
			m_av = averagePar(mu, mr, ti, te, tx);
			pval = p0(l_av, m_av, stateChangeModel.getGamma(), ti, te, pval);
		}
		return pval;
	}

	/**
	 * Calculates the log-likelihood of an edge at time edge_ti in state, given that state started at time tx and
	 * p(edge_te)=pval
	 *
	 * @param edge_ti the start of the edge
	 * @param edge_te the time of the initial condition (=end of edge)
	 * @param pval the value of the initial condition
	 * @param state the current state
	 * @param tx the starting time of state
	 * @param gamma state change rate
	 * @return the log likelihood
	 */
	private double step_q(double edge_ti, double edge_te, double pval, int state, double tx, double gamma) {
		double logPedge = 0;
		double lambda = stateChangeModel.getLambda(state);
		double lr = stateChangeModel.getLambdaRate(state);
		double mu = stateChangeModel.getMu(state);
		double mr = stateChangeModel.getMuRate(state);

		int nsteps = (int) Math.ceil((edge_ti - edge_te) / stepsize);
		double step = (edge_ti - edge_te) / nsteps;
		double l_av, m_av;

		for (int s = 0; s < nsteps; s++) {
			double step_ti = edge_te + (s + 1) * step;
			double step_te = edge_te + s * step;
			l_av = averagePar(lambda, lr, step_ti, step_te, tx);
			m_av = averagePar(mu, mr, step_ti, step_te, tx);

			logPedge += logfratio(l_av, m_av, gamma, step_ti, step_te, step_te, pval);
			pval = p0(l_av, m_av, gamma, step_ti, step_te, pval);
		}
		return logPedge;
	}

	/**
	 * Gets the times at which calculation of p is needed for the current state.
	 *
	 * @param node the current node
	 * @param i the index of the current state change
	 * @param times the array of times
	 * @return the updated array of times
	 */
	private ArrayList<Triple> getDescendingTimes(MultiRateNode node, int i, ArrayList<Triple> times) {
		if (i > 0) {
			Triple triple = new Triple(node.getChangeTime(i - 1), node.getNr(), i);
			times.add(triple);
		} else if (i == 0) {
			Triple triple = new Triple(node.getHeight(), node.getNr(), i);
			times.add(triple);
			if (node.isLeaf()) return times;

			for (Node child : node.getChildren()) {
				times = getDescendingTimes((MultiRateNode) child, -1, times);
			}
		} else {
			times = getDescendingTimes(node, node.getChangeCount(), times);
		}
		return times;
	}

	/**
	 * The Class Triple contains a 3-elements tuple.
	 */
	class Triple implements Comparable<Triple> {
		public Double e1;
		public Integer e2, e3;

		/**
		 * Instantiates a new triple.
		 *
		 * @param x the 1st element
		 * @param y the 2nd element
		 * @param z the 3rd element
		 */
		public Triple(double x, int y, int z) {
			e1 = x;
			e2 = y;
			e3 = z;
		}

		@Override
		public int compareTo(Triple o) {
			return e1.compareTo(o.e1);
		}
	}
}