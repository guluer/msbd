/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multiratetree.operators;

import org.apache.commons.math.MathException;

import beast.core.Input;
import beast.evolution.tree.ExpStateChangeModel;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.Node;
import beast.math.distributions.ParametricDistribution;
import beast.util.Randomizer;

/**
 * Operator to add/remove states from model and tree. A state can only be removed if it is only present in one
 * contiguous place in the tree, excluding the root.
 */
public class AddRemoveColourWPropagation extends RandomPropagationRetypeOperator {

	public Input<ParametricDistribution> lambdaDistributionInput = new Input<>("lambdaDistribution",
			"Distribution from which to draw a new speciation rate");
	public Input<ParametricDistribution> muDistributionInput = new Input<>("muDistribution",
			"Distribution from which to draw a new extinction rate");
	public Input<ParametricDistribution> lambdaRDistributionInput = new Input<>("lambdaRateDistribution",
			"Distribution from which to draw a new speciation decay rate");
	public Input<ParametricDistribution> muRDistributionInput = new Input<>("muRateDistribution",
			"Distribution from which to draw a new extinction decay rate");

	public Input<Double> addPropInput = new Input<>("addProp", "Proportion of add moves (tuning parameter), default 0.5", 0.5);

	ParametricDistribution lambdaDist, muDist, lambdaRDist, muRDist;
	private boolean ldraw = true, mdraw = true, lrdraw = true, mrdraw = true, isExp = false;
	double add_prop;
	boolean add;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		lambdaDist = lambdaDistributionInput.get();
		muDist = muDistributionInput.get();
		lambdaRDist = lambdaRDistributionInput.get();
		muRDist = muRDistributionInput.get();

		if (stateChangeModel.isLambdaFixed()) ldraw = false;
		else if (lambdaDist == null) throw new IllegalStateException("No distribution provided for drawing lambdas");
		if (stateChangeModel.isMuFixed()) mdraw = false;
		else if (muDist == null) throw new IllegalStateException("No distribution provided for drawing mus");

		if (stateChangeModel instanceof ExpStateChangeModel) {
			isExp = true;
			if (((ExpStateChangeModel) stateChangeModel).isLambdaRateFixed()) lrdraw = false;
			else if (lambdaRDist == null) throw new IllegalStateException("No distribution provided for drawing lambda rates");
			if (((ExpStateChangeModel) stateChangeModel).isMuRateFixed()) mrdraw = false;
			else if (muRDist == null) throw new IllegalStateException("No distribution provided for drawing mu rates");
		}

		add_prop = addPropInput.get();
	}

	@Override
	public double proposal() {
		double logHR;
		add = Randomizer.nextDouble() < add_prop;
		try {
			if (add) logHR = proposal_add();
			else logHR = proposal_remove();
		} catch (Exception e) {
			return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

	/**
	 * Propose a colour removal.
	 *
	 * @return the logHR of the move
	 */
	private double proposal_remove() {
		int ns = stateChangeModel.getNStates();
		if (ns == 1) return Double.NEGATIVE_INFINITY;
		double logHR = 0.0;

		logHR += Math.log(add_prop) - Math.log(1 - add_prop);

		int[] counts = occurrences();
		// special case : we will not remove the root color even if it only occurs on the root
		counts[((MultiRateNode) mrTree.getRoot()).getNodeState()] = 2;
		int x = 0;
		for (int i = 0; i < counts.length; i++) {
			// only contiguous colours are considered (counts==0 not used because of reversibility)
			if (counts[i] == 1) x++;
		}

		if (x == 0) return Double.NEGATIVE_INFINITY;
		logHR += Math.log(x);
		int color;
		do {
			color = Randomizer.nextInt(ns);
		} while (counts[color] != 1);

		int k = -1;
		MultiRateNode node = null;
		for (Node node2 : mrTree.getNodesAsArray()) {
			if (node2.isRoot()) continue;
			node = (MultiRateNode) node2;
			if (node.getNodeState() == color && node.getChangeCount() > 0) k = 0;
			for (int i = 0; i < node.getChangeCount() - 1; i++) {
				if (node.getChangeState(i) == color) {
					k = i + 1;
					break;
				}
			}
			if (k != -1) break;
		}

		logHR += retypeBranchForRemoveNoStop(node, k);
		if (ldraw) logHR += lambdaDist.logDensity(stateChangeModel.getLambda(color));
		if (mdraw) logHR += muDist.logDensity(stateChangeModel.getMu(color));
		if (isExp) {
			if (lrdraw) logHR += lambdaRDist.logDensity(((ExpStateChangeModel) stateChangeModel).getLambdaRate(color));
			if (mrdraw) logHR += muRDist.logDensity(((ExpStateChangeModel) stateChangeModel).getMuRate(color));
		}
		removeState(color);

		// inverse contributions for reverse move
		int n = mrTree.getLeafNodeCount();
		int m = mrTree.getTotalNumberOfChanges();
		logHR -= Math.log(2 * n - 2 + m);

		return logHR;
	}

	/**
	 * Propose a colour addition.
	 *
	 * @return the logHR of the move
	 * @throws MathException if sampling from one distribution fails
	 */
	private double proposal_add() throws MathException {
		int ns = stateChangeModel.getNStates();
		if (ns == stateChangeModel.getNstar()) return Double.NEGATIVE_INFINITY;

		double logHR = 0.0;
		logHR -= Math.log(add_prop) - Math.log(1 - add_prop);

		int n = mrTree.getLeafNodeCount();
		int m = mrTree.getTotalNumberOfChanges();
		// Select sub-edge at random:
		int k = Randomizer.nextInt(2 * n - 2 + m);
		logHR += Math.log(2 * n - 2 + m);

		MultiRateNode node = null;
		for (Node node2 : mrTree.getNodesAsArray()) {
			if (node2.isRoot()) continue;
			node = (MultiRateNode) node2;
			if (k < node.getChangeCount() + 1) {
				break;
			}
			k -= node.getChangeCount() + 1;
		}

		double newL = 0, newM = 0;
		if (ldraw) {
			newL = lambdaDist.sample(1)[0][0];
			logHR -= lambdaDist.logDensity(newL);
		}
		if (mdraw) {
			newM = muDist.sample(1)[0][0];
			logHR -= muDist.logDensity(newM);
		}

		if (!isExp) stateChangeModel.addState(newL, newM);
		else {
			double newLR = 0, newMR = 0;
			if (lrdraw) {
				newLR = lambdaRDist.sample(1)[0][0];
				logHR -= lambdaRDist.logDensity(newLR);
			}
			if (mrdraw) {
				newMR = muRDist.sample(1)[0][0];
				logHR -= muRDist.logDensity(newMR);
			}
			((ExpStateChangeModel) stateChangeModel).addState(newL, newM, newLR, newMR);
		}

		int newstate = ns;
		int oldstate;
		if (k == 0) oldstate = node.getNodeState();
		else oldstate = node.getChangeState(k - 1);

		double[] t = node.getTimeRangeBelow(k);
		double changet = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
		node.insertChange(k, oldstate, changet);

		logHR += retypeBranchForAdd(node, k, newstate, false) + Math.log(t[1] - t[0]); // HR

		// here calculation of x needed and added to logHR
		int[] counts = occurrences();
		// special case : we will not remove the root color even if it only occurs on the root
		counts[((MultiRateNode) mrTree.getRoot()).getNodeState()] = 2;
		int x = 0;
		for (int i = 0; i < counts.length; i++) {
			if (counts[i] == 1) x++;
		}
		logHR -= Math.log(x);

		return logHR;
	}

	@Override
	public void reject(final int reason) {
		super.reject(reason);
		if (reason > -2) {
			if (add) stateChangeModel.setNStates(stateChangeModel.getNStates() - 1);
			else stateChangeModel.setNStates(stateChangeModel.getNStates() + 1);
		}
	}
}
