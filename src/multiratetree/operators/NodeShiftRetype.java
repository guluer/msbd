/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multiratetree.operators;

import beast.core.Input;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.Node;
import beast.util.Randomizer;

/**
 * Change the state and height of a random node and retype all edges connected to that node, using the uniformization
 * retype method (adapted from the MultiTypeTree package).
 *
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public class NodeShiftRetype extends UniformizationRetypeOperator {

	public Input<Boolean> rootOnlyInput = new Input<>("rootOnly", "Always select root node for height adjustment.", false);

	public Input<Boolean> noRootInput = new Input<>("noRoot", "Never select root node for height adjustment.", false);

	public Input<Double> rootScaleFactorInput = new Input<>("rootScaleFactor", "Scale factor used in root height proposals. (Default 0.8)", 0.8);

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		if (rootOnlyInput.get() && noRootInput.get()) throw new IllegalArgumentException("rootOnly and noRoot inputs " + "cannot both be set to true simultaneously.");
	}

	@Override
	public double proposal() {
		// Select internal node to adjust:
		Node node;
		if (rootOnlyInput.get()) node = mrTree.getRoot();
		else do {
			node = mrTree.getNode(Randomizer.nextInt(mrTree.getNodeCount()));
		} while (noRootInput.get() && node.isRoot());

		// Generate relevant proposal:
		if (node.isRoot()) return rootProposal(node);
		else return nonRootProposal(node);
	}

	/**
	 * Proposal applied to the root.
	 *
	 * @param root the root
	 * @return log of HR
	 */
	private double rootProposal(Node root) {

		double logHR = 0.0;

		// Record probability of current typing:
		logHR += getBranchStatesProb(root.getLeft()) + getBranchStatesProb(root.getRight());

		// Select new root height:
		double u = Randomizer.nextDouble();
		double f = u * rootScaleFactorInput.get() + (1 - u) / rootScaleFactorInput.get();
		double oldestChildHeight = Math.max(root.getLeft().getHeight(), root.getRight().getHeight());
		root.setHeight(oldestChildHeight + f * (root.getHeight() - oldestChildHeight));
		logHR -= Math.log(f);

		// Select new root node state:
		((MultiRateNode) root).setNodeState(Randomizer.nextInt(stateChangeModel.getNStates()));

		// Recolour branches below root:
		try {
			logHR -= retypeBranch(root.getLeft()) + retypeBranch(root.getRight());
		} catch (NoValidPathException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

	/**
	 * Proposal applied to a non-root node.
	 *
	 * @param node the node
	 * @return log of HR of move
	 */
	private double nonRootProposal(Node node) {

		double logHR = 0.0;

		// Record probability of current colouring:
		logHR += getBranchStatesProb(node);
		if (!node.isLeaf()) {
			logHR += getBranchStatesProb(node.getLeft()) + getBranchStatesProb(node.getRight());
		}

		// Select new node height:
		double upperBound = node.getParent().getHeight();
		double lowerBound;
		if (node.isLeaf()) {
			lowerBound = 0;
		} else {
			lowerBound = Math.max(node.getLeft().getHeight(), node.getRight().getHeight());
		}
		node.setHeight(lowerBound + (upperBound - lowerBound) * Randomizer.nextDouble());

		// Select new node colour:
		((MultiRateNode) node).setNodeState(Randomizer.nextInt(stateChangeModel.getNStates()));

		// Recolour branches connected to node:
		try {
			logHR -= retypeBranch(node);
			if (!node.isLeaf()) {
				logHR -= retypeBranch(node.getLeft()) + retypeBranch(node.getRight());
			}
		} catch (NoValidPathException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

}
