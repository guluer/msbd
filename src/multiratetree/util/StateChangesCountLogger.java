/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multiratetree.util;

import java.io.PrintStream;

import beast.core.CalculationNode;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.MultiRateTree;
import beast.evolution.tree.Node;

/**
 * Log the number of rate shifts in the tree.
 */
public class StateChangesCountLogger extends CalculationNode implements Loggable {

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree whose changes will be counted.",
			Validate.REQUIRED);

	private MultiRateTree mrTree;

	@Override
	public void initAndValidate() {
		mrTree = multiRateTreeInput.get();
	}

	@Override
	public void init(PrintStream out) {
		String idString = mrTree.getID();
		out.print(idString + "count_of_changes" + "\t");
	}

	@Override
	public void log(long nSample, PrintStream out) {
		int count = 0;

		for (Node node : mrTree.getNodesAsArray()) {
			if (node.isRoot()) continue;
			count += ((MultiRateNode) node).getChangeCount();
		}

		out.print(count + "\t");
	}

	@Override
	public void close(PrintStream out) {}

}
