/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package multiratetree.util;

import java.io.PrintStream;
import java.util.Arrays;

import beast.core.CalculationNode;
import beast.core.Function;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.MultiRateTree;
import beast.evolution.tree.Node;
import beast.evolution.tree.StateChangeModel;

/**
 * Log the number of state changes present in the tree for each possible change i->j.
 */
public class StateChangesMatrixLogger extends CalculationNode implements Loggable, Function {

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree whose changes will be counted.",
			Validate.REQUIRED);

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Rate shift model needed to specify number of states.",
			Validate.REQUIRED);

	private MultiRateTree mrTree;

	private int nstates;

	private int[] stateChanges;
	private boolean dirty;


	@Override
	public void initAndValidate() {
		mrTree = multiRateTreeInput.get();
		nstates = stateChangeModelInput.get().getNStates();

		stateChanges = new int[nstates * (nstates - 1)];

		dirty = true;
		update();
	}

	/**
	 * Update state change count array as necessary.
	 */
	private void update() {
		if (!dirty) return;

		// Zero state change count array
		nstates = stateChangeModelInput.get().getNStates();
		stateChanges = new int[nstates * (nstates - 1)];
		Arrays.fill(stateChanges, 0);

		// Recalculate array elements
		for (Node node : mrTree.getNodesAsArray()) {
			if (node.isRoot()) {
				continue;
			}

			MultiRateNode mtNode = (MultiRateNode) node;
			int laststate = mtNode.getNodeState();
			for (int i = 0; i < mtNode.getChangeCount(); i++) {
				int nextstate = mtNode.getChangeState(i);
				stateChanges[getOffset(laststate, nextstate)] += 1;
				laststate = nextstate;
			}
		}

	}

	/**
	 * Retrieve offset into state change count array.
	 *
	 * @param i from state (forwards)
	 * @param j to state (forwards)
	 * @return offset
	 */
	private int getOffset(int i, int j) {
		if (i == j) throw new RuntimeException(
				"Programmer error: requested state " + "change count array offset for diagonal element of " + "state change count matrix.");

		if (i > j) i -= 1;
		return j * (nstates - 1) + i;
	}

	/**
	 * Get dimension of the array containing the counts
	 * 
	 * @return size of the array
	 * @see beast.core.Function#getDimension()
	 */
	@Override
	public int getDimension() {
		return nstates * (nstates - 1);
	}

	@Override
	public double getArrayValue() {
		update();
		return stateChanges[0];
	}

	@Override
	public double getArrayValue(int iDim) {
		if (iDim < getDimension()) {
			update();
			return stateChanges[iDim];
		} else return Double.NaN;
	}

	@Override
	public void init(PrintStream out) {

		String idString = mrTree.getID();

		for (int state = 0; state < nstates; state++) {
			for (int stateP = 0; stateP < nstates; stateP++) {
				if (state == stateP) {
					continue;
				}
				out.print(idString + ".count_" + state + "_to_" + stateP + "\t");
			}
		}
	}

	@Override
	public void log(long nSample, PrintStream out) {
		update();

		for (int state = 0; state < nstates; state++) {
			for (int stateP = 0; stateP < nstates; stateP++) {
				if (state == stateP) {
					continue;
				}
				out.print(stateChanges[getOffset(state, stateP)] + "\t");
			}
		}
	}

	@Override
	public void close(PrintStream out) {}

	@Override
	public boolean requiresRecalculation() {
		dirty = true;
		return true;
	}

}
