package multiratetree.util;

import java.util.ArrayList;
import java.util.List;

import beast.core.Function;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;
import beast.core.parameter.RealParameter;
import beast.evolution.tree.MultiRateNode;
import beast.evolution.tree.MultiRateTree;
import beast.evolution.tree.Node;
import beast.evolution.tree.StateChangeModel;

public class TreeWithParameterValuesLogger extends MRTWithMetaDataLogger implements Loggable {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Model of transition between states.",
			Validate.REQUIRED);

	StateChangeModel scModel;

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		scModel = stateChangeModelInput.get();
	}

	@Override
	List<Function> getMetadata(MultiRateTree tree) {
		List<Function> metadata = new ArrayList<Function>();

		// add parameter values to metadata
		Double[] lambdas = new Double[tree.getNodeCount()];
		Double[] mus = new Double[tree.getNodeCount()];

		for (Node n : tree.getNodesAsArray()) {
			int st = ((MultiRateNode) n).getNodeState();
			lambdas[n.getNr()] = scModel.getLambda(st);
			mus[n.getNr()] = scModel.getMu(st);
		}

		RealParameter lambdasMD = new RealParameter(lambdas);
		lambdasMD.setID("lambda");
		RealParameter musMD = new RealParameter(mus);
		musMD.setID("mu");
		metadata.add(lambdasMD);
		metadata.add(musMD);

		return(metadata);
	}

}
